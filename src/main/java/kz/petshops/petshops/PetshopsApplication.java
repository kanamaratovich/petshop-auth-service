package kz.petshops.petshops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PetshopsApplication {

	public static void main(String[] args) {
		SpringApplication.run(PetshopsApplication.class, args);
	}

}
